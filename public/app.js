
$(function(){

    $('.modal-close, .modal-background').click( function(){

        if ( $(this).parents('.modal').find('.is-loading').length ) return false;
        $(this).parents('.modal').removeClass('is-active').find('.new').remove();
    });

    $(document).keyup( function(e){

        $('.is-loading').removeClass('is-loading');
        if ( e.keyCode === 27 ) $('.modal-close').click();
    });

    $('a[href^="#"]').click( function(){

        var $target = $($(this).attr('href'));
        var $form = $target.find('form');
        $target.addClass('is-active');
        $form.show();

        var $new_form = $($form.parent().html()).addClass('new');

        if ( $(this).is('.add') ){

            $new_form.attr('action', $form.attr('action').split('/').slice(0,-1).join('/'));
            if ( !$new_form.attr('action') ) $new_form.attr('action', '/');
            
            $new_form.find('input').val('');
            $new_form.find('.submit').text('add');
            $new_form.find('.toggle').removeClass('readonly').find('input').attr('checked', false);
            $new_form.find('[name="refund"]').attr('disabled', true);
    
        } else {

            $new_form.find('[name="number"]').attr('readonly', true);
            $new_form.find('[name="amount"]').attr('readonly', true);
        }

        $form.hide().parent().append($new_form);

        return false;
    });

    $(document).submit('form', function(target){

        var $form = $(target.target);

        $form.find('.error').remove();
        $form.find('.submit').addClass('is-loading').attr('disabled', true);

        return true;
    });

    $(document).submit('#card form', function(target){

        var $form = $(target.target);

        if ( ! $form.is('#card form') ) return true;
        if ( $form.find('[name="number"]').attr('readonly') ) return true;
        if ( $form.find('[name="source"]').val() ) return true;

        Stripe.card.createToken({

            number: $form.find('[name="number"]').val(),
            exp_month: $form.find('[name="exp_month"]').val(),
            exp_year: $form.find('[name="exp_year"]').val(),
            cvc: $form.find('[name="cvc"]').val(),

        }, function(status, response){

            if ( response.error ) {

                var message = $('<div class="field"><article class="error message is-danger is-hazy"><div class="message-body"></div></article></div>');
                message.find('.message-body').text(response.error.message);
                $form.find('.column').first().prepend(message);
                $form.find('button').removeClass('is-loading').attr('disabled', false);

                return false;
            }

            $form.append( $('<input type="hidden" name="source">').val(response.id) );
            $form.submit();
        });

        return false;
    });

});

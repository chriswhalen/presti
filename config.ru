require 'dotenv/load'
require 'haml'
require 'sinatra'
require 'stripe'

require './app'

set :show_exceptions, false
enable :sessions

run Sinatra::Application

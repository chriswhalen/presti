presti
======

![Preview](preview.png)

**presti** is a simple responsive web dashboard for interacting with your
[Stripe](https://stripe.com) account.


def form( *parameters )

    request = {}
    parameters.each do | p |
        request["#{ p }"] = params["#{ p }"] if params["#{ p }"]
    end
    
    request

end


def flash( message=nil )

    session[:flash] = message if message
    session[:flash]

end


not_found do

    redirect to '/'

end


error do

    haml ".content.is-size-4.has-text-danger #{ env['sinatra.error'].message }"

end


before do

    @stripe_js_api_key = session[:js_api_key] ? session[:js_api_key] : ENV['PUBLIC_KEY']
    Stripe.api_key = session[:api_key] ? session[:api_key] : ENV['SECRET_KEY']

    begin

        customers = Stripe::Customer.list limit: 100
        @customers = []
        customers.each do | c |
            @customers << c if c.description
        end

    rescue Exception, StandardError

        if not params['api_key']
            raise 'Invalid secret key.'
        end

    end

end


after do
    
    session[:flash] = nil if request.get?
    
end


get '/' do

    haml :index

end


get '/:customer' do

    @customer = nil
    @customers.each do | c |
        @customer = c if c.id == params['customer']
    end

    return not_found if not @customer

    haml :index

end


get '/:customer/:card' do

    @customer = nil
    @customers.each do | c |
        @customer = c if c.id == params['customer']
    end

    return not_found if not @customer

    @card = nil
    @customer.sources.each do | c |
        @card = c if c.id == params['card']
    end

    return redirect to "/#{ @customer.id }" if not @card

    @charges = []
    Stripe::Charge.list().data.each do | c |
        @charges << c if c['source']['id'] == @card.id
    end

    haml :index

end


get '/:customer/:card/:charge' do

    @customer = nil
    @customers.each do | c |
        @customer = c if c.id == params['customer']
    end

    return not_found if not @customer

    @card = nil
    @customer.sources.each do | c |
        @card = c if c.id == params['card']
    end

    return redirect to "/#{ @customer.id }" if not @card

    @charge = nil
    @charges = []
    Stripe::Charge.list().data.each do | c |
        @charge = c if c['id'] == params['charge']
        @charges << c if c['source']['id'] == @card.id
    end

    return redirect to "/#{ @customer.id }/#{ @card.id }" if not @charge

    haml :index

end


post '/' do

    fields = form :description, :email

    begin

        @customer = Stripe::Customer.create fields if not fields.empty?

    rescue Stripe::StripeError => e

            flash e.json_body[:error][:message]
            return redirect to "/#{ @customer.id }"

    end

    redirect to "/#{ @customer.id }"

end


post '/api' do

    session[:api_key] = params['api_key'] if params['api_key']
    session[:js_api_key] = params['js_api_key'] if params['js_api_key']

    redirect to '/'

end


post '/:customer' do

    @customer = nil
    @customers.each do | c |
        @customer = c if c.id == params['customer']
    end

    return not_found if not @customer

    fields = form :description, :email

    Stripe::Customer.update @customer.id, fields if not fields.empty?

    fields = form :source

    if not fields.empty?

        fields['metadata'] = form :number, :cvc

        begin

            @card = Stripe::Customer.create_source @customer.id, fields

        rescue Stripe::StripeError => e

            flash e.json_body[:error][:message]
            return redirect to "/#{ @customer.id }"

        end

        return redirect to "/#{ @customer.id }/#{ @card.id }"

    end

    redirect to "/#{ @customer.id }"

end


post '/:customer/delete' do

    @customer = nil
    @customers.each do | c |
        @customer = c if c.id == params['customer']
    end

    return not_found if not @customer

    @customer.delete

    redirect to "/"

end


post '/:customer/:card' do

    @customer = nil
    @customers.each do | c |
        @customer = c if c.id == params['customer']
    end

    return not_found if not @customer

    @card = nil
    @customer.sources.each do | c |
        @card = c if c.id == params['card']
    end

    return redirect to "/#{ @customer.id }" if not @card

    fields = form :exp_month, :exp_year, :cvc

    if not fields.empty?

        fields['metadata'] = JSON.parse @card.metadata.to_json
        fields['metadata']['cvc'] = fields['cvc']
        fields['name'] = @customer.description
        fields.delete 'cvc'

        begin

            @card = Stripe::Customer.update_source @customer.id, @card.id, fields

        rescue Stripe::StripeError => e

            flash e.json_body[:error][:message]
            return redirect to "/#{ @customer.id }"

        end

        return redirect to "/#{ @customer.id }/#{ @card.id }"

    end

    fields = form :amount

    if not fields.empty?

        fields = form :amount, :capture

        begin

            fields['amount'] = (fields['amount'].to_f * 100).round().to_i
            fields['capture'] = !!fields['capture']
            fields['currency'] = @card.country == 'CA' ? 'cad' : 'usd'
            fields['customer'] = @customer.id
            fields['source'] = @card.id

            @charge = Stripe::Charge.create fields

            return redirect to "/#{ @customer.id }/#{ @card.id }/#{ @charge.id }"

        rescue Stripe::StripeError => e

            flash e.json_body[:error][:message]
            return redirect to "/#{ @customer.id }/#{ @card.id }"

        end

    end

    redirect to "/#{ @customer.id }"

end


post '/:customer/:card/delete' do

    @customer = nil
    @customers.each do | c |
        @customer = c if c.id == params['customer']
    end

    return not_found if not @customer

    @card = nil
    @customer.sources.each do | c |
        @card = c if c.id == params['card']
    end

    return redirect to "/#{ @customer.id }" if not @card

    @card.delete

    redirect to "/#{ @customer.id }"

end


post '/:customer/:card/:charge' do

    @customer = nil
    @customers.each do | c |
        @customer = c if c.id == params['customer']
    end

    return not_found if not @customer

    @card = nil
    @customer.sources.each do | c |
        @card = c if c.id == params['card']
    end

    return redirect to "/#{ @customer.id }" if not @card

    @charge = nil
    @charges = []
    Stripe::Charge.list().data.each do | c |
        @charge = c if c['id'] == params['charge']
        @charges << c if c['source']['id'] == @card.id
    end

    return redirect to "/#{ @customer.id }/#{ @card.id }" if not @charge

    fields = form :capture, :refund

    begin

        @charge.capture if not @charge.captured and fields['capture']

        Stripe::Refund.create charge: @charge if not @charge.refunded and fields['refund']
  
    rescue Stripe::StripeError => e

        flash e.json_body[:error][:message]

    end

    redirect to "/#{ @customer.id }/#{ @card.id }/#{ @charge.id }"

end
